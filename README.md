# Description

This repository provides hardening recommendations for frp (https://github.com/fatedier/frp)

- patch to improve security between client and server (tls1.3, ed25519) 
- container images are build from scratch to remove any dependencies
