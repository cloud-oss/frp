#!/bin/sh
set -e

if [ "$DEBUG" ]; then
   set -ux
fi

# functions
docker_pull() {
    docker pull "$1" | grep -e 'Pulling from' -e Digest -e Status -e Error;
}

## load env vars
source build/build.args

## base images
for BASE in $(grep "FROM" build/Dockerfile | grep -v scratch | cut -d" " -f2 | sort | uniq)
do
   eval docker_pull "$BASE"
done

## model build-args property
BUILD_ARGS=$(while IFS= read -r line; do printf "%s" "--build-arg $line "; done < build/build.args)

## build image
docker build --no-cache --force-rm $BUILD_ARGS -t frp-artifacts:"$FRP" build/
docker build --no-cache --force-rm $BUILD_ARGS --build-arg BUILDERIMAGE=frp-artifacts:"$FRP" -f build/Dockerfile.client -t frpc:"$FRP" build/
docker build --no-cache --force-rm $BUILD_ARGS --build-arg BUILDERIMAGE=frp-artifacts:"$FRP" -f build/Dockerfile.server -t frps:"$FRP" build/
